package com.pvsoft.swoosh.Controllers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.pvsoft.swoosh.Model.Player
import com.pvsoft.swoosh.R
import com.pvsoft.swoosh.Utilities.EXTRA_LEAGUE
import com.pvsoft.swoosh.Utilities.EXTRA_PLAYER
import com.pvsoft.swoosh.Utilities.EXTRA_SKILL
import kotlinx.android.synthetic.main.activity_skill.*

class SkillActivity : AppCompatActivity() {

//    var league = ""
//    var skill = ""
//    lateinit var player: Player

    var player = Player("","")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skill)
//        league = intent.getStringExtra(EXTRA_LEAGUE)
        player = intent.getParcelableExtra(EXTRA_PLAYER)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EXTRA_PLAYER, player)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if(savedInstanceState != null) {
            player = savedInstanceState.getParcelable(EXTRA_PLAYER)
        }
    }

    fun beginnerClicked(view: View) {
        ballerBtn.isChecked = false
//        skill = "beginner"
        player.skill = "beginner"
    }

    fun ballerClicked(view: View) {
        beginnerBtn.isChecked = false
//        skill = "baller"
        player.skill = "baller"
    }

    fun finishClicked(view: View) {
//        if(skill != null) {
        if(player.skill != "") {
            val finishActivity = Intent(this,FinishActivity::class.java)
//            finishActivity.putExtra(EXTRA_LEAGUE, league)
//            finishActivity.putExtra(EXTRA_SKILL, skill)
            finishActivity.putExtra(EXTRA_PLAYER, player)
            startActivity(finishActivity)
        } else {
            Toast.makeText(this, "Please select your skill!", Toast.LENGTH_SHORT).show()
        }
    }
}
