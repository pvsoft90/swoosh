package com.pvsoft.swoosh.Controllers

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import com.pvsoft.swoosh.Model.Player
import com.pvsoft.swoosh.R
import com.pvsoft.swoosh.Utilities.EXTRA_LEAGUE
import com.pvsoft.swoosh.Utilities.EXTRA_PLAYER
import kotlinx.android.synthetic.main.activity_league.*

class LeagueActivity : BaseActivity() {

//    var leagueValue: String = ""
    lateinit var player: Player // Chỉ khai báo, sẽ khởi tạo sau

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)
        player = Player("","")
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putParcelable(EXTRA_PLAYER, player)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if(savedInstanceState != null) {
            player = savedInstanceState.getParcelable(EXTRA_PLAYER)
        }
    }

    fun mensClicked(view: View) {
//        leagueValue = "mens"
        player = Player("mens","")
        womensLeagueBtn.isChecked = false
        coedLeagueBtn.isChecked = false
    }

    fun womensClicked(view: View) {
//        leagueValue = "womens"
        player = Player("womens","")
        mensLeagueBtn.isChecked = false
        coedLeagueBtn.isChecked = false
    }

    fun coedClicked(view: View) {
//        leagueValue = "coed"
        player = Player("coed","")
        mensLeagueBtn.isChecked = false
        womensLeagueBtn.isChecked = false
    }

    fun leagueNextClicked(view: View) {
        if(player.league != "") {
            val skillActivity = Intent(this, SkillActivity::class.java)
//            skillActivity.putExtra(EXTRA_LEAGUE, leagueValue)
            skillActivity.putExtra(EXTRA_PLAYER, player)
            startActivity(skillActivity)
        } else {
            Toast.makeText(this,"Please select League!", Toast.LENGTH_SHORT).show()
        }

    }
}
