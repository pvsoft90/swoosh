package com.pvsoft.swoosh.Controllers

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.pvsoft.swoosh.Model.Player
import com.pvsoft.swoosh.R
import com.pvsoft.swoosh.Utilities.EXTRA_LEAGUE
import com.pvsoft.swoosh.Utilities.EXTRA_PLAYER
import com.pvsoft.swoosh.Utilities.EXTRA_SKILL
import kotlinx.android.synthetic.main.activity_finish.*

class FinishActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finish)

//        val league = intent.getStringExtra(EXTRA_LEAGUE)
//        val skill = intent.getStringExtra(EXTRA_SKILL)
//        searchLeagueText.text = "Looking for $league $skill league near you..."

        val player = intent.getParcelableExtra<Player>(EXTRA_PLAYER)
        searchLeagueText.text = "Looking for ${player.league} ${player.skill} league near you..."
    }
}
